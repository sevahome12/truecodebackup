import google_api
import yandex_api
import schedule
import time

def job():
    print("I'm working...")
    google_api.main()
    yandex_api.main()

schedule.every().thursday.at("05:51").do(job)
        
while True: #Запуск цикла
    schedule.run_pending()
    time.sleep(1)