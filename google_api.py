import os
from google.oauth2 import service_account
from googleapiclient.http import MediaIoBaseDownload,MediaFileUpload
from googleapiclient.discovery import build
from pathlib import Path
import pprint
import config
import io
from os.path import getctime
from datetime import datetime as dt
def get_email(res):
        for i in range(len(res['permissions'])):
                if res['permissions'][i]['role'] == 'owner':
                        email = res['permissions'][i]['emailAddress']
                        return email

def get_path(file: dict, files: list, path=''):
    if 'parents' in file.keys():
        parent = [f for f in files if f['id'] == file['parents'][0]][0]
        path = get_path(parent, files, path) + '/' + parent['name']
    return path

def download_files(results, service):
        for res in results['files']:
                try:
                        i = res['permissions']
                except:
                        continue
                file_id = res['id']
                if(res['mimeType'] != 'application/vnd.google-apps.document'):
                        request = service.files().get_media(fileId=file_id)
                else:
                        request = service.files().export_media(fileId=file_id, mimeType='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                
                email = get_email(res)
        #СОЗДАНИЕ ЛИЧНОЙ ПАПКИ ПОЛЬЗОВАТЕЛЯ ЕСЛИ ЕЁ НЕТ
                if not os.path.exists('test/'+email):
                        os.mkdir('test/'+email)

                if res['mimeType'] == 'application/vnd.google-apps.folder':
                        continue
                path = get_path(res, results['files'])
                if not os.path.exists('test/'+email+path):
                        os.makedirs('test/'+email+path)
                filename = 'test/'+email + path+'/'+res['name']
                
                if os.path.exists(filename) and dt.fromtimestamp(getctime(filename)).strftime('%Y-%m-%dT%H:%M:%S') >= res['modifiedTime'][:-5]:
                        continue
                fh = io.FileIO(filename, 'wb')
                downloader = MediaIoBaseDownload(fh, request)

                done = False
                try:
                        while done is False:
                                status, done = downloader.next_chunk()
                                #print ("Download %d%%." % int(status.progress() * 100))
                                #print (filename)
                except:
                        #print('skip')
                        continue
def main():
        pp = pprint.PrettyPrinter(indent=4)
        SCOPES = ['https://www.googleapis.com/auth/drive']
        SERVICE_ACCOUNT_FILE = config.GoToken

        credentials = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=SCOPES)
        service = build('drive', 'v3', credentials=credentials)

        results = service.files().list(pageSize=10, fields="nextPageToken, files(id, name, mimeType, permissions, parents, modifiedTime)").execute()
        nextPageToken = results.get('nextPageToken')
        while nextPageToken:
                nextPage = service.files().list(pageSize=10, fields="nextPageToken, files(id, name, mimeType, permissions, parents, modifiedTime)",                                       pageToken=nextPageToken).execute()
                nextPageToken = nextPage.get('nextPageToken')
                results['files'] = results['files'] + nextPage['files']
        #pp.pprint(results)
        #print(len(results.get('files')))
        download_files(results,service)